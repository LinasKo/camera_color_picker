import json
import os.path


class JsonManager:
    def __init__(self):
        pass

    @staticmethod
    def save_values(filename, values):
        """
        Overrides a file and saves given python data structure as JSON.
        Note: not tested extensively. Be careful with numpy values and such.

        :param filename: filename to save to.
        :param values: python data structure to be saved.
        """
        with open(filename, 'w') as outfile:
            json.dump(values, outfile, indent=4)

    @staticmethod
    def get_values(filename):
        """
        Returns the saved values from a file
        :param filename: Name of the file to look for.
        """

        if os.path.isfile(filename):
            with open(filename, 'r') as infile:
                return json.load(infile)
        else:
            return None
