#!/usr/bin/env python
"""
Visual Color Picker
===================

Usage:
Click:          Select color to track
CTRL Click:     Select min color to track
SHIFT Click:    Select max color to track

s:              Save current color calibration for the object
o:              Change object type for which colors are calibrated
n:              Display next image if loaded from file
Space:          Quit

"""
import argparse
import cv2
import numpy as np
from persistence_manager import JsonManager
from trackbar import Trackbar
from visual_input import Camera
from visual_input import ImageReader

"""
When a mouse click is registered on the resulting:

The min color values will be set to:
[pixel[H] - H_RADIUS, pixel[S] - S_RADIUS, pixel[V] - V_RADIUS]

The max color values will be set to:
[pixel[H] + H_RADIUS, pixel[S] + S_RADIUS, pixel[V] + V_RADIUS]

All the colors between min and max values will be detected.
"""
H_RADIUS = 10
S_RADIUS = 80
V_RADIUS = 80

"""
When a CTRL-click click is registered on the resulting image,
The min color values will be set to:
[pixel[H] - H_NOISE, pixel[S] - S_NOISE, pixel[V] - V_NOISE]

When an SHIFT-click click is registered on the resulting image,
The max color values will be set to:
[pixel[H] + H_NOISE, pixel[S] + S_NOISE, pixel[V] + V_NOISE]
"""
H_NOISE = 5
S_NOISE = 10
V_NOISE = 10

"""
You can calibrate the colors for different objects. Cycle through object types by using the keyboard.
"""
OBJECT_TYPES = ["Flower", "Cat", "Finnegan's Wake"]

CALIBRATIONS_FILE = "color_calibrations.json"


class GUI:
    def __init__(self, camera):
        self.camera = camera

        self.frame = np.zeros((300, 512, 3), np.uint8)
        self.min_image = np.zeros((200, 200, 3), np.uint8)
        self.max_image = np.zeros((200, 200, 3), np.uint8)

        self.current_object_type = 0

        self.main_window_name = "Camera Calibration"
        cv2.namedWindow(self.main_window_name)
        cv2.namedWindow("Min Color")
        cv2.namedWindow("Max Color")
        cv2.namedWindow("Output Window")

        self.trackbars = [
            Trackbar("H_min", self.main_window_name, max_value=179),
            Trackbar("S_min", self.main_window_name),
            Trackbar("V_min", self.main_window_name),
            Trackbar("H_max", self.main_window_name, max_value=179),
            Trackbar("S_max", self.main_window_name),
            Trackbar("V_max", self.main_window_name),
        ]

        for trackbar in self.trackbars:
            trackbar.create()

        self.calibrations = {}
        for object_type in OBJECT_TYPES:
            self.calibrations[object_type] = {trackbar.name: trackbar.get_value() for trackbar in self.trackbars}

        cv2.setMouseCallback("Output Window", self.mouse_click)

        self.load_colors()
        print "Currently setting color for \"%s\"." % OBJECT_TYPES[self.current_object_type]

    def loop(self):
        # Update images with loaded slider values.
        self.save_colors()

        while True:
            key = cv2.waitKey(1) & 0xFF

            # Shut down
            if key == ord(" "):
                self.close()
                break

            # Cycle through recorded pictures
            elif hasattr(self.camera, "next_image") and key == ord("n"):
                self.camera.next_image()

            # Save the color value
            elif key == ord("s"):
                self.save_colors()

            # Change object whose color will be set
            elif key == ord("o"):
                self.current_object_type = (self.current_object_type + 1) % len(OBJECT_TYPES)
                self._reload_trackers()
                print "Currently setting color for \"%s\"." % OBJECT_TYPES[self.current_object_type]

            self.update_colors()
            self.update_frame()

    def save_colors(self):
        """
        Save the current color values to a json file.
        """
        new_colors = {trackbar.name: trackbar.get_value() for trackbar in self.trackbars}

        self.calibrations[OBJECT_TYPES[self.current_object_type]] = new_colors
        JsonManager.save_values(CALIBRATIONS_FILE, self.calibrations)
        print "Colors of \"%s\" saved!" % OBJECT_TYPES[self.current_object_type]

    def load_colors(self, filename=CALIBRATIONS_FILE):
        """
        Loads and sets the current color values from a file.

        :param filename:    Path to file to load the colors from.
        """
        saved_calibrations = JsonManager.get_values(filename)
        if saved_calibrations:
            for object_type in OBJECT_TYPES:
                if object_type in saved_calibrations:
                    self.calibrations[object_type] = saved_calibrations[object_type]
            self._reload_trackers()

    def _reload_trackers(self):
        """
        Trackers need to be updated whenever changes of self.calibrations need to be displayed.
        """
        for trackbar in self.trackbars:
            current_object = OBJECT_TYPES[self.current_object_type]
            trackbar.set_pos(self.calibrations[current_object][trackbar.name])
        self.update_colors()

    def update_colors(self):
        """
        Update the displayed color values based on the trackbar positions.
        """
        new_colors = {trackbar.name: trackbar.get_value() for trackbar in self.trackbars}

        current_object = OBJECT_TYPES[self.current_object_type]
        if new_colors != self.calibrations[current_object]:
            self.calibrations[current_object] = new_colors

        # TODO: sort our the hardcoded strings here and onwards
        rgb_min = cv2.cvtColor(np.uint8([[[new_colors["H_min"], new_colors["S_min"], new_colors["V_min"]]]]),
                               cv2.COLOR_HSV2BGR)
        rgb_max = cv2.cvtColor(np.uint8([[[new_colors["H_max"], new_colors["S_max"], new_colors["V_max"]]]]),
                               cv2.COLOR_HSV2BGR)

        self.min_image[:] = rgb_min
        self.max_image[:] = rgb_max

        cv2.imshow("Min Color", self.min_image)
        cv2.imshow("Max Color", self.max_image)

    def update_frame(self):
        """
        Mask the selected regions
        """
        self.frame = self.camera.get_frame()
        hsv_frame = cv2.cvtColor(self.frame, cv2.COLOR_BGR2HSV)

        # define range of color to show
        min_color = np.array([
            self.calibrations[OBJECT_TYPES[self.current_object_type]]["H_min"],
            self.calibrations[OBJECT_TYPES[self.current_object_type]]["S_min"],
            self.calibrations[OBJECT_TYPES[self.current_object_type]]["V_min"]
        ])
        max_color = np.array([
            self.calibrations[OBJECT_TYPES[self.current_object_type]]["H_max"],
            self.calibrations[OBJECT_TYPES[self.current_object_type]]["S_max"],
            self.calibrations[OBJECT_TYPES[self.current_object_type]]["V_max"]
        ])

        # Threshold the HSV image to get the mask, close it, and get the overlay.
        # For actual procedure done in the crab, refer to   capture/preprocess.py:149
        mask_kernel = np.ones((5, 5), np.uint8)
        mask = cv2.bitwise_not(cv2.inRange(hsv_frame, min_color, max_color))

        # Overlay with a bright zone, to highlight the detected locations
        inv_closed_mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, mask_kernel)
        closed_mask = cv2.bitwise_not(inv_closed_mask)
        hsv_green = np.uint8([[[0, 255, 0]]])
        green_image = np.ones_like(self.frame) * hsv_green
        green_overlay = cv2.bitwise_and(green_image, green_image, mask=closed_mask)

        # Bitwise-AND mask and original image
        output_frame = cv2.bitwise_and(self.frame, self.frame, mask=inv_closed_mask)
        output_frame += green_overlay

        cv2.imshow("Output Window", output_frame)

    # noinspection PyUnusedLocal
    def mouse_click(self, event, x, y, flags, param):
        pixel = cv2.cvtColor(self.frame, cv2.COLOR_BGR2HSV)[y][x]

        if event == cv2.EVENT_LBUTTONDOWN and (flags & cv2.EVENT_FLAG_CTRLKEY):
            cv2.setTrackbarPos("H_min", self.main_window_name, max(0, pixel[0] - H_NOISE))
            cv2.setTrackbarPos("S_min", self.main_window_name, max(0, pixel[1] - S_NOISE))
            cv2.setTrackbarPos("V_min", self.main_window_name, max(0, pixel[2] - V_NOISE))

        elif event == cv2.EVENT_LBUTTONDOWN and (flags & cv2.EVENT_FLAG_SHIFTKEY):
            cv2.setTrackbarPos("H_max", self.main_window_name, min(pixel[0] + H_NOISE, 179))
            cv2.setTrackbarPos("S_max", self.main_window_name, min(pixel[1] + S_NOISE, 255))
            cv2.setTrackbarPos("V_max", self.main_window_name, min(pixel[2] + V_NOISE, 255))

        elif event == cv2.EVENT_LBUTTONDOWN:
            cv2.setTrackbarPos("H_min", self.main_window_name, max(0, pixel[0] - H_RADIUS))
            cv2.setTrackbarPos("S_min", self.main_window_name, max(0, pixel[1] - S_RADIUS))
            cv2.setTrackbarPos("V_min", self.main_window_name, max(0, pixel[2] - V_RADIUS))

            cv2.setTrackbarPos("H_max", self.main_window_name, min(pixel[0] + H_RADIUS, 179))
            cv2.setTrackbarPos("S_max", self.main_window_name, min(pixel[1] + S_RADIUS, 255))
            cv2.setTrackbarPos("V_max", self.main_window_name, min(pixel[2] + V_RADIUS, 255))

    def close(self):
        self.camera.close()
        cv2.destroyAllWindows()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--source", help="Which camera source to use. Default is 0", type=int)
    parser.add_argument("--recorded", help="Uses a set of recorded images, instead of video.", action="store_true")

    args = parser.parse_args()

    if args.recorded:
        image_folder = "./saved_images/"
        number_of_images = 3
        cam = ImageReader(image_folder, number_of_images)
    else:
        cam = Camera(source=args.source)

    print __doc__

    gui = GUI(cam)
    gui.loop()
