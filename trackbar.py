#!/usr/bin/env python
import cv2


class Trackbar:
    """
    Wrapping into an encapsulating class in order to reduce code repetition
    """
    def __init__(self, name, window_name, max_value=255):
        """
        :param name:        trackbar name, used as an id to refer to it
        :param window_name: window that the tracker will be located in
        """
        self.name = name
        self.window_name = window_name
        self.max_value = max_value

    @staticmethod
    def nothing(_):
        """
        OpenCV seems to require a function type for the trackbars
        """
        pass

    def create(self):
        """
        Creates the trackbar
        """
        cv2.createTrackbar(self.name, self.window_name, 0, self.max_value, self.nothing)

    def set_pos(self, value):
        cv2.setTrackbarPos(self.name, self.window_name, value)

    def get_value(self):
        return cv2.getTrackbarPos(self.name, self.window_name)
