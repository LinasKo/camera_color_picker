#!/usr/bin/env python
import cv2


class ImageReader:
    def __init__(self, image_folder, number_of_images):
        """
        Reads stored images, instead of using the camera
        """
        self.image_folder = image_folder
        self.image_index = 0
        self.number_of_images = number_of_images

    def next_image(self):
        self.image_index = (self.image_index + 1) % self.number_of_images

    def get_frame(self):
        return cv2.imread(self.image_folder + "image" + str(self.image_index) + ".jpg")

    def close(self):
        pass


class Camera:
    def __init__(self, source=0):
        print "Using camera %d." % source

        # noinspection PyArgumentList
        self.capture = cv2.VideoCapture(source)

    def get_frame(self):
        _, frame = self.capture.read()
        return frame

    def close(self):
        if hasattr(self, "capture"):
            self.capture.release()
