However, I have not asked whether different SDP teams can collaborate on a
single vision system, so either get permission for that or keep any edits you
make to yourself.

​

If you are not doing SDP but still wish to contribute, drop me an email at
linaskondrackis@gmail.com