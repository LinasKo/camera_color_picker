This is a side product of my honours project. I figured, it'd be useful for anyone
who needs to detect different color regions in an image or a live feed.

In particular, it'll hopefully be useful for the robot football project (SDP) in
University of Edinburgh. I've received permission from Prof. Webb to release this publicly
so it could be used for that purpose this year (2016-2017).

​

It is assumed that you have OpenCV installed (Should work with both 2.4.x and 3.x)

Edit: one amazing guide - http://www.pyimagesearch.com/2016/10/24/ubuntu-16-04-how-to-install-opencv/

​

**Usage:**

```color_calibration.py --source <camera number>``` - launch color calibration using
a live feed. All camera numbers you should need are 0 and 1.

```color_calibration.py --recorded``` - launch color calibration using recorded images.

​

Some more parameters, such as image locations are in the source files.  
Further instructions are shown upon launching the program.

In the following example picture you can see the detected colors marked in green.
![](example1.png)